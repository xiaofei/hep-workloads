# HEP Workloads list


## To run

* Docker

```
docker run -v /path/to/local/folder:/results $DOCKER_IMAGE:latest
```
* Apptainer/Singularity

```
chmod a+rw /path/to/local/folder
apptainer run -B /path/to/local/folder:/results $SIF_IMAGE:latest_$arch
```
where:
 * arch=x86_64 or arch=aarch64
 * DOCKER_IMAGE and SIF_IMAGE path can be found in the below table, columns SIF/Docker image registry


| Experiment |  WL repo  | SIF image registry | Docker image registry| Latest Built Version | Latest Pipeline status | Unpacked container size | 
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| alice | [digi-reco-core-run3-ma][alice_digi-reco-core-run3-ma_code] | [click for link][alice_digi-reco-core-run3-ma_sif] | [click for link][alice_digi-reco-core-run3-ma_img] | [v2.2][alice_digi-reco-core-run3-ma_pipelink] | ![ci][alice_digi-reco-core-run3-ma_pipeline]|       14G       |
| atlas | [gen_sherpa-ma][atlas_gen_sherpa-ma_code] | [click for link][atlas_gen_sherpa-ma_sif] | [click for link][atlas_gen_sherpa-ma_img] | [v2.1][atlas_gen_sherpa-ma_pipelink] | ![ci][atlas_gen_sherpa-ma_pipeline]|        22G        |
| atlas | [kv][atlas_kv_code] | [click for link][atlas_kv_sif] | [click for link][atlas_kv_img] | [ci2.0][atlas_kv_pipelink] | ![ci][atlas_kv_pipeline]|    -n    |
| atlas | [reco_mt-ma][atlas_reco_mt-ma_code] | [click for link][atlas_reco_mt-ma_sif] | [click for link][atlas_reco_mt-ma_img] | [v2.3][atlas_reco_mt-ma_pipelink] | ![ci][atlas_reco_mt-ma_pipeline]|   22G   |
| atlas | [sim_mt-ma][atlas_sim_mt-ma_code] | [click for link][atlas_sim_mt-ma_sif] | [click for link][atlas_sim_mt-ma_img] | [v2.1][atlas_sim_mt-ma_pipelink] | ![ci][atlas_sim_mt-ma_pipeline]|  20G  |
| belle2 | [gen-sim-reco-ma][belle2_gen-sim-reco-ma_code] | [click for link][belle2_gen-sim-reco-ma_sif] | [click for link][belle2_gen-sim-reco-ma_img] | [v2.2][belle2_gen-sim-reco-ma_pipelink] | ![ci][belle2_gen-sim-reco-ma_pipeline]|             3.4G             |
| cms | [digi-run3-ma][cms_digi-run3-ma_code] | [click for link][cms_digi-run3-ma_sif] | [click for link][cms_digi-run3-ma_img] | [v1.1][cms_digi-run3-ma_pipelink] | ![ci][cms_digi-run3-ma_pipeline]|          5.8G          |
| cms | [gen-sim-run3-ma][cms_gen-sim-run3-ma_code] | [click for link][cms_gen-sim-run3-ma_sif] | [click for link][cms_gen-sim-run3-ma_img] | [v1.1][cms_gen-sim-run3-ma_pipelink] | ![ci][cms_gen-sim-run3-ma_pipeline]|                        6.2G                        |
| cms | [hlt-ma][cms_hlt-ma_code] | [click for link][cms_hlt-ma_sif] | [click for link][cms_hlt-ma_img] | [v0.2][cms_hlt-ma_pipelink] | ![ci][cms_hlt-ma_pipeline]|                19G                |
| cms | [mlpf][cms_mlpf_code] | [click for link][cms_mlpf_sif] | [click for link][cms_mlpf_img] | [v0.1][cms_mlpf_pipelink] | ![ci][cms_mlpf_pipeline]|                                                    |
| cms | [reco-run3-ma][cms_reco-run3-ma_code] | [click for link][cms_reco-run3-ma_sif] | [click for link][cms_reco-run3-ma_img] | [v1.2][cms_reco-run3-ma_pipelink] | ![ci][cms_reco-run3-ma_pipeline]|           6.5G           |
| hello | [world-c7-ma][hello_world-c7-ma_code] | [click for link][hello_world-c7-ma_sif] | [click for link][hello_world-c7-ma_img] | [v1.0][hello_world-c7-ma_pipelink] | ![ci][hello_world-c7-ma_pipeline]|   759M   |
| hello | [world-cs8-ma][hello_world-cs8-ma_code] | [click for link][hello_world-cs8-ma_sif] | [click for link][hello_world-cs8-ma_img] | [ci-v1.0][hello_world-cs8-ma_pipelink] | ![ci][hello_world-cs8-ma_pipeline]|              518M              |
| igwn | [pe][igwn_pe_code] | [click for link][igwn_pe_sif] | [click for link][igwn_pe_img] | [v0.5][igwn_pe_pipelink] | ![ci][igwn_pe_pipeline]|     2.9G     |
| juno | [gen-sim-reco][juno_gen-sim-reco_code] | [click for link][juno_gen-sim-reco_sif] | [click for link][juno_gen-sim-reco_img] | [v3.0][juno_gen-sim-reco_pipelink] | ![ci][juno_gen-sim-reco_pipeline]| 3.3G |
| lhcb | [sim-run3-ma][lhcb_sim-run3-ma_code] | [click for link][lhcb_sim-run3-ma_sif] | [click for link][lhcb_sim-run3-ma_img] | [v1.1][lhcb_sim-run3-ma_pipelink] | ![ci][lhcb_sim-run3-ma_pipeline]|            5.4G            |
| mg5amc | [madgraph4gpu-2022][mg5amc_madgraph4gpu-2022_code] | [click for link][mg5amc_madgraph4gpu-2022_sif] | [click for link][mg5amc_madgraph4gpu-2022_img] | [ci-v0.10][mg5amc_madgraph4gpu-2022_pipelink] | ![ci][mg5amc_madgraph4gpu-2022_pipeline]|                    11G                    |

[alice_digi-reco-core-run3-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/alice/digi-reco-core-run3-ma/alice-digi-reco-core-run3-ma
[alice_digi-reco-core-run3-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=alice-digi-reco-core-run3-ma-bmk
[alice_digi-reco-core-run3-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=alice-digi-reco-core-run3-ma-bmk
[alice_digi-reco-core-run3-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6470423
[alice_digi-reco-core-run3-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-alice-digi-reco-core-run3-ma/pipeline.svg

[atlas_gen_sherpa-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/atlas/gen_sherpa-ma/atlas-gen_sherpa-ma
[atlas_gen_sherpa-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=atlas-gen_sherpa-ma-bmk
[atlas_gen_sherpa-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=atlas-gen_sherpa-ma-bmk
[atlas_gen_sherpa-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6462115
[atlas_gen_sherpa-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-atlas-gen_sherpa-ma/pipeline.svg

[atlas_kv_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/atlas/kv/atlas-kv
[atlas_kv_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=atlas-kv-bmk
[atlas_kv_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=atlas-kv-bmk
[atlas_kv_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6481046
[atlas_kv_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-atlas-kv/pipeline.svg

[atlas_reco_mt-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/atlas/reco_mt-ma/atlas-reco_mt-ma
[atlas_reco_mt-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=atlas-reco_mt-ma-bmk
[atlas_reco_mt-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=atlas-reco_mt-ma-bmk
[atlas_reco_mt-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6624381
[atlas_reco_mt-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-atlas-reco_mt-ma/pipeline.svg

[atlas_sim_mt-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/atlas/sim_mt-ma/atlas-sim_mt-ma
[atlas_sim_mt-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=atlas-sim_mt-ma-bmk
[atlas_sim_mt-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=atlas-sim_mt-ma-bmk
[atlas_sim_mt-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6638701
[atlas_sim_mt-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-atlas-sim_mt-ma/pipeline.svg

[belle2_gen-sim-reco-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/belle2/gen-sim-reco-ma/belle2-gen-sim-reco-ma
[belle2_gen-sim-reco-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=belle2-gen-sim-reco-ma-bmk
[belle2_gen-sim-reco-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=belle2-gen-sim-reco-ma-bmk
[belle2_gen-sim-reco-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6442812
[belle2_gen-sim-reco-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-belle2-gen-sim-reco-ma/pipeline.svg

[cms_digi-run3-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/cms/digi-run3-ma/cms-digi-run3-ma
[cms_digi-run3-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=cms-digi-run3-ma-bmk
[cms_digi-run3-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=cms-digi-run3-ma-bmk
[cms_digi-run3-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6457275
[cms_digi-run3-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-cms-digi-run3-ma/pipeline.svg

[cms_gen-sim-run3-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/cms/gen-sim-run3-ma/cms-gen-sim-run3-ma
[cms_gen-sim-run3-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=cms-gen-sim-run3-ma-bmk
[cms_gen-sim-run3-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=cms-gen-sim-run3-ma-bmk
[cms_gen-sim-run3-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/5668234
[cms_gen-sim-run3-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-cms-gen-sim-run3-ma/pipeline.svg

[cms_hlt-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/cms/hlt-ma/cms-hlt-ma
[cms_hlt-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=cms-hlt-ma-bmk
[cms_hlt-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=cms-hlt-ma-bmk
[cms_hlt-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6284528
[cms_hlt-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-cms-hlt-ma/pipeline.svg

[cms_mlpf_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/cms/mlpf/cms-mlpf
[cms_mlpf_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=cms-mlpf-bmk
[cms_mlpf_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=cms-mlpf-bmk
[cms_mlpf_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/4175345
[cms_mlpf_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-cms-mlpf/pipeline.svg

[cms_reco-run3-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/cms/reco-run3-ma/cms-reco-run3-ma
[cms_reco-run3-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=cms-reco-run3-ma-bmk
[cms_reco-run3-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=cms-reco-run3-ma-bmk
[cms_reco-run3-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6452700
[cms_reco-run3-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-cms-reco-run3-ma/pipeline.svg

[hello_world-c7-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/hello/world-c7-ma/hello-world-c7-ma
[hello_world-c7-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=hello-world-c7-ma-bmk
[hello_world-c7-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=hello-world-c7-ma-bmk
[hello_world-c7-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6516852
[hello_world-c7-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-hello-world-c7-ma/pipeline.svg

[hello_world-cs8-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/hello/world-cs8-ma/hello-world-cs8-ma
[hello_world-cs8-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=hello-world-cs8-ma-bmk
[hello_world-cs8-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=hello-world-cs8-ma-bmk
[hello_world-cs8-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6442131
[hello_world-cs8-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-hello-world-cs8-ma/pipeline.svg

[igwn_pe_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/igwn/pe/igwn-pe
[igwn_pe_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=igwn-pe-bmk
[igwn_pe_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=igwn-pe-bmk
[igwn_pe_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6475749
[igwn_pe_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-igwn-pe/pipeline.svg

[juno_gen-sim-reco_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/juno/gen-sim-reco/juno-gen-sim-reco
[juno_gen-sim-reco_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=juno-gen-sim-reco-bmk
[juno_gen-sim-reco_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=juno-gen-sim-reco-bmk
[juno_gen-sim-reco_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6743071
[juno_gen-sim-reco_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-juno-gen-sim-reco/pipeline.svg

[lhcb_sim-run3-ma_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/lhcb/sim-run3-ma/lhcb-sim-run3-ma
[lhcb_sim-run3-ma_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=lhcb-sim-run3-ma-bmk
[lhcb_sim-run3-ma_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=lhcb-sim-run3-ma-bmk
[lhcb_sim-run3-ma_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6446812
[lhcb_sim-run3-ma_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-lhcb-sim-run3-ma/pipeline.svg

[mg5amc_madgraph4gpu-2022_code]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/blob/master/mg5amc/madgraph4gpu-2022/mg5amc-madgraph4gpu-2022
[mg5amc_madgraph4gpu-2022_sif]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads-sif/container_registry/?search%5B%5D=mg5amc-madgraph4gpu-2022-bmk
[mg5amc_madgraph4gpu-2022_img]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/container_registry/?search%5B%5D=mg5amc-madgraph4gpu-2022-bmk
[mg5amc_madgraph4gpu-2022_pipelink]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/-/pipelines/6100700
[mg5amc_madgraph4gpu-2022_pipeline]: https://gitlab.cern.ch/hep-benchmarks/hep-workloads/badges/qa-build-mg5amc-madgraph4gpu-2022/pipeline.svg

