#!/bin/bash -ex

function install_prmon(){
    yum install -y git;
    cd /tmp
    git clone -b v3.0.2 --recurse-submodules https://github.com/HSF/prmon.git; 
    mkdir /tmp/build; cd /tmp/build; 
    
    
    yum install -y cmake3 make gcc-c++ python3 ; 
    
    if [ `grep -c "CentOS Linux release 7" /etc/redhat-release` -eq 1 ]; then 
        yum install -y centos-release-scl 
        yum install -y devtoolset-10
        scl enable devtoolset-10 'cmake3 -DCMAKE_INSTALL_PREFIX=/usr /tmp/prmon; make -j4; make install'
    else 
        cmake3 -DCMAKE_INSTALL_PREFIX=/usr /tmp/prmon; make -j4; make install
    fi

    cd /tmp; rm -rf build prmon;
    yum install -y stress
    prmon -- stress --cpu 2 --timeout 20
    cat prmon.json 
    yum clean all;
}


# do not for slc6 because of BMK-671
yum repolist
if [ `grep -c "Scientific Linux CERN" /etc/redhat-release` -ne 1 ]; then 
    yum install -y  centos-release epel-release ; 
    yum install -y  unzip bzip2 which man file util-linux gcc wget tar freetype perl jq && 
    yum clean all; 
fi

# Prepare a data directory for downloading large files that should normally be cacheable (BMK-159)
# Its contents should be retrieved in Dockerfile.append, before /bmk/<bmkdir> is copied over
# Each file it contains is then individually symlinked to /bmk/<bmkdir>/data/<file> in Dockerfile.template
mkdir -p /bmk/data

install_prmon

