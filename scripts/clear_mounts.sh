#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

function usage(){
  echo "Usage: $(basename $0)"
  exit 1
}

if [ "$1" == "" ]; then
  MOUNTS=$(df |& grep 'Transport endpoint is not connected' | awk '{print substr($2,2,length($2)-3)}')
else
  usage
fi

###echo $MOUNTS

if [ "$MOUNTS" != "" ]; then
  echo "Clearing mounts:"
  echo "$MOUNTS"
  umount $MOUNTS
else
  echo There are no mounts to clear
fi

