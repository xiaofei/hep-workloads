# These variables are needed in generate_Dockerfile.sh
HEPWL_BMKEXE=test-ci-bmk.sh
HEPWL_BMKDIR=test-ci
HEPWL_BMKDESCRIPTION="DUMMY benchmark for CI tests (based on LHCb setup)"
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/cc7-base:20221201-1.x86_64" # see https://gitlab.cern.ch/linuxsupport/cc7-base/container_registry; test the CI using cc7-base:latest (default is slc6-base:latest)

# These variables are needed in main.sh
###HEPWL_BMKOPTS="-c 1" # DUMMY LHCb setup TEST (DEFAULT)
HEPWL_BMKOPTS="-c 2 --extra-args '--hallo-world --setup-lhcb'" # DUMMY HalloWorld and LHCb setup TESTS (BMK-1048)
###HEPWL_BMKOPTS="-c 1 --extra-args '--hallo-world'" # DUMMY HalloWorld TEST (FASTER)
###HEPWL_BMKOPTS="-c 1 --extra-args '--setup-lhcb'" # DUMMY LHCb setup TEST (DEFAULT)
###HEPWL_BMKOPTS="-c 1 --extra-args '--setup-gauss'" # DUMMY LHCb Gauss setup TEST (SLOWER)
###HEPWL_BMKOPTS="-c 200" # Debug BMK-247 using 200 copies of the default test (DEBUG)
HEPWL_DOCKERIMAGENAME=test-ci-bmk
HEPWL_DOCKERIMAGETAG=ci0.3 # versions >= ci0.3 use optional EXTRA_ARGS
HEPWL_CVMFSREPOS=lhcb.cern.ch
