#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Global variables
export CMSSW_RELEASE=CMSSW_12_5_0
export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch

ARCH=`uname -p`
echo "Detected CPU architecture $ARCH"
if [[ ${ARCH} =~ (^x86_(32|64)$) ]]; then
  export SCRAM_ARCH=el8_amd64_gcc10
elif [[ ${ARCH} =~ (^aarch64$) ]]; then
  export SCRAM_ARCH=el8_aarch64_gcc10
else
    echo "ERROR! This script is only supported on x86 and aarch64 architectures"
    exit 1
fi


# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra CMS-DIGI-specific setup
  echo "value CMSSW_RELEASE=$CMSSW_RELEASE"
  echo "value VO_CMS_SW_DIR=$VO_CMS_SW_DIR"
  echo "value SCRAM_ARCH=$SCRAM_ARCH"
  source $VO_CMS_SW_DIR/cmsset_default.sh
  [[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
  pushd ${CMSSW_RELEASE}; eval `scramv1 runtime -sh`; popd
  # Configure WL copy
  ln -s ${BMKDIR}/data/GlobalTag.db ./GlobalTag.db
  ln -s ${BMKDIR}/data/*.root .
  CMSSW_CONF=tt_digi_DIGI_DATAMIX_L1_DIGI2RAW_HLT.py
  JOB_EVENTS=$(( NEVENTS_THREAD * NTHREADS )) # bash shell arithmetic, may use var instead of $var
  cp ${BMKDIR}/${CMSSW_CONF}_template ./${CMSSW_CONF}
  sed -e "s@_NEVENTS_@${JOB_EVENTS}@g" -e "s@_NTHREADS_@$NTHREADS@g" -i ./${CMSSW_CONF}
  # Execute WL copy
  LOG=out_$1.log
  cmsRun ./${CMSSW_CONF} >>$LOG 2>&1 3>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Function that retrieves from the input file the number of events
# in order to check that there are enough events for the number of running threads
function getEvents(){
  currentDir=`pwd`
  [[ ! -e /tmp/getEvents ]] && mkdir /tmp/getEvents
  cd /tmp/getEvents
  echo "[getEvents] value CMSSW_RELEASE=$CMSSW_RELEASE"
  echo "[getEvents] value VO_CMS_SW_DIR=$VO_CMS_SW_DIR"
  echo "[getEvents] value SCRAM_ARCH=$SCRAM_ARCH"

  source $VO_CMS_SW_DIR/cmsset_default.sh
  echo "[getEvents] value SCRAM_ARCH=$SCRAM_ARCH"
  [[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE} 
  pushd ${CMSSW_RELEASE}; eval `scramv1 runtime -sh`; popd

  eventfile=${BMKDIR}/data/gensim.root
  echo "[getEvents] Checking number of events in file $eventfile"
  unset TERM  #needed otherwise the python call will add an escape sequence https://incenp.org/notes/2012/python-term-smm-fix.html
  evts=`python3 -c "import ROOT;f=ROOT.TFile.Open('$eventfile');print('%d' % f.Get('Events').GetEntries());"`
  STATUS=$?

  cd $currentDir
  rm -rf /tmp/getEvents 
  [[ $STATUS != 0 ]] && evts=0
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$((`nproc`/$NCOPIES))
  fi
  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Extract number of events in input file
  declare -i evts=0
  getEvents
  echo "[validateInputArguments] number of events in input file is ${evts}"
  if [[ $evts == 0 ]] ; then
      s_msg="[ERROR] Cannot extract number of events from input file"
      return 1
  fi
  if [[ $((NEVENTS_THREAD * NTHREADS)) -gt $evts ]] ; then
      s_msg="[ERROR] Requested number of events ($((NEVENTS_THREAD * NTHREADS))) exceeds number of input events ($evts)"
      return 1
  fi
  # Return 0 if input arguments are valid, 1 otherwise
  # Report any issues to parseResults via s_msg
  export s_msg="ok"
  tot_load=$(($NCOPIES*$NTHREADS))
  if [ $tot_load -gt `nproc` ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load > number of available cores (`nproc`)"
    return 1
  elif [ $tot_load -eq 0 ]; then
    s_msg="[ERROR] NCOPIES*NTHREADS=$NCOPIES*$NTHREADS=$tot_load. Please fix it"
    return 1
  elif [ $tot_load -ne `nproc` ];
    then s_msg="[WARNING] NCOPIES*NTHREADS ($NCOPIES*$NTHREADS=$tot_load) != `nproc` (number of available cores nproc)"
    echo $s_msg
  fi
  return 0
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=4
NCOPIES=$(( `nproc` / $NTHREADS ))
NEVENTS_THREAD=100
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > nproc
  NCOPIES=1
  NTHREADS=`nproc`
fi

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
