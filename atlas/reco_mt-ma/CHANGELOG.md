# Changelog for Atlas recoMT

## [Unreleased]

## [v2.3] 2023-12-15
- Minor improvement of the script

## [v2.2] 2023-12-07
- Validate number of input events

## [v2.1] 2023-11-10
- Limit the number of loaded cores using the argument -n | --ncores 

## [v2.0] 2022-11-03
- First working release of Atlas recoMT built for x86_64 and aarch64
- The version starts from v2.0 to avoid confusion with the legacy images built in atlas/sim_mt having version v1.x
- This release is based on Athena 23.0.3 and is different w.r.t. the workload built in atlas/sim_mt that is based on Athena 22.0.27