# Run arguments file auto-generated on Wed Sep  7 08:24:16 2022 by:
# JobTransform: EVNTtoHITS
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'EVNTtoHITS' 

runArgs.perfmon = 'fastmonmt'
runArgs.maxEvents = 6
runArgs.geometryVersion = 'ATLAS-R3S-2021-03-00-00'
runArgs.AMITag = 's3873'
runArgs.postInclude = ['RecJobTransforms/UseFrontier.py']
runArgs.preInclude = ['Campaigns/MC21SimulationMultiBeamSpot.py', 'SimulationJobOptions/preInclude.ExtraParticles.py', 'SimulationJobOptions/preInclude.G4ExtraProcesses.py']
runArgs.firstEvent = 6160001
runArgs.physicsList = 'FTFP_BERT_ATL'
runArgs.randomSeed = 6163
runArgs.conditionsTag = 'OFLCOND-MC21-SDR-RUN3-05'
runArgs.truthStrategy = 'MC15aPlus'
runArgs.simulator = 'FullG4MT_QS'
runArgs.jobNumber = 1

 # Input data
runArgs.inputEVNTFile = ['/cvmfs/atlas.cern.ch/repo/benchmarks/hep-workloads/input-data/EVNT.13043099._000859.pool.root.1']
runArgs.inputEVNTFileType = 'EVNT'
runArgs.inputEVNTFileNentries = 10000
runArgs.EVNTFileIO = 'input'

 # Output data
runArgs.outputHITSFile = 'myHITS.pool.root'
runArgs.outputHITSFileType = 'HITS'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets

 # Executor flags
runArgs.totalExecutorSteps = 0
