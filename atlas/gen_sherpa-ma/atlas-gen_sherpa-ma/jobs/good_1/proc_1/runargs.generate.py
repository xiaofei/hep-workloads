# Run arguments file auto-generated on Thu Oct 13 21:04:04 2022 by:
# JobTransform: generate
# Version: $Id: trfExe.py 792052 2017-01-13 13:36:51Z mavogel $
# Import runArgs class
from PyJobTransforms.trfJobOptions import RunArguments
runArgs = RunArguments()
runArgs.trfSubstepName = 'generate' 

runArgs.perfmon = 'fastmonmt'
runArgs.ecmEnergy = 13000.0
runArgs.firstEvent = 1
runArgs.randomSeed = 12345
runArgs.printEvts = 5
runArgs.generatorRunMode = 'run'
runArgs.generatorJobNumber = 0
runArgs.lheOnly = 0
runArgs.cleanOut = 0
runArgs.VERBOSE = False
runArgs.ignoreBlackList = False
runArgs.jobConfig = ['/bmk/atlas-gen_sherpa-ma/auxfiles']
runArgs.maxEvents = 4

 # Input data

 # Output data
runArgs.outputEVNTFile = 'test_sherpa_tttautau.EVNT.pool.root'
runArgs.outputEVNTFileType = 'EVNT'

 # Extra runargs

 # Extra runtime runargs

 # Literal runargs snippets

 # Executor flags
runArgs.totalExecutorSteps = 0
