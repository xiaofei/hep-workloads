HEPWL_BMKEXE=alice-digi-reco-core-run3-ma-bmk.sh
HEPWL_BMKOPTS="-t 4 -c 1 -e 1 -m none"
HEPWL_BMKDIR=alice-digi-reco-core-run3-ma
HEPWL_BMKDESCRIPTION="ALICE Run3 PbPb DIGI-RECO (core)"
HEPWL_DOCKERIMAGENAME=alice-digi-reco-core-run3-ma-bmk
HEPWL_DOCKERIMAGETAG=v2.2
HEPWL_CVMFSREPOS=alice.cern.ch
HEPWL_BMKOS="centos:centos7.9.2009"
HEPWL_EXTEND_ALICE_SPEC=./alice_spec_custom.txt
HEPWL_BUILDARCH="x86_64,aarch64"
HEPWL_DOCKER_EXTRA_ARGS="--shm-size 8000000000"
