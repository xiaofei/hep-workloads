# Changelog for Alice digi-reco-core-run3-ma

## [Unreleased]

## [v2.2] 2023-11-13
- Limit the number of loaded cores using the argument -n | --ncores 

## [v2.1] 2023-02-13
- Fix socket conflicts

## [v2.0] 2023-01-31
- First working release of Alice digi reco run3 for x86_64 and aarch64

