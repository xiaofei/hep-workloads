# Changelog for Juno gen-sim-reco

## [Unreleased]

## [v3.0] 2023-12-21
- Update to latest JUNO version J23.1.0-rc5.dc1. This version of juno software integrate 4 steps event processing into 2 steps.
Another feature is output detail event processing time.
- Reduce gen-sim memory usage. But the RECO memory usage is still more than 2GB.
- Exclude the initial time of juno event processing.
- Fix the problem of junodb connections.

## [v2.2] 2023-11-13
- Limit the number of loaded cores using the argument -n | --ncores

## [v2.1] 2022-11-19
- Rebuild with the new CI/CD pipelines

## [v2.0] 2022-07-01
- use elapsed time instead of user time as reported metric

## [v1.1] 2021-12-16
- Apply same parser base template to all workloads

## [v1.0] 2021-11-03
- First working release of Juno gen-sim-reco
