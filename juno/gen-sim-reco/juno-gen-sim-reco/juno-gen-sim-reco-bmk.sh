#!/bin/bash

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  echo "[doOne ($1)] do one! (process $1 of $NCOPIES)"

  # Configure WL copy
  curdir=`pwd`
  echo "Current directory: $curdir"
  # Suppress unnecessary warning message
  #export CMTCONFIG=amd64_linux26
  source /cvmfs/juno.ihep.ac.cn/centos7_amd64_gcc1120/Pre-Release/J23.1.0-rc5.dc1/setup.sh > /dev/null
  #export CMTCONFIG=amd64_linux26
  # Execute WL copy
  echo "Executing the following number of threads:"
  echo $NTHREADS

  # run N+1 event, and only calculate 2 to N+1 event time. Exclude the fisrt enven time.
  let  EVT_NUM=${NEVENTS_THREAD}+1
  

  # Ignore requests for multi-threading
  echo "#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*  Start Gen-sim:  #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*"
  python $TUTORIALROOT/share/tut_detsim.py --no-ion-php --profiling --profiling-details --evtmax $EVT_NUM gun > $curdir/output.sim
  
  # get total  event+1 time
  echo "############## list after GEN-SIM  directory  ##################"
  ls -alhs
  sim_total1=`cat output.sim |grep "Sum of junotoptask"|awk '{print $5}'`
  # get the first gen-sim event time
  sim_event1=`cat junotoptas* |head -n 2|tail -n 1|awk '{print $3}'`
  # get total gen-sim event time = sum - first
  sim_totalms=`echo "$sim_total1-$sim_event1"|bc`
  sim_totals=$(echo "scale=3; $sim_totalms/1000"|bc)
  # out  put gen-sim event time in second  
  echo $sim_totals > $curdir/outputsim
  echo "SIM Time: $sim_totals"
  mv junotoptas* outputsimprofile

  echo "#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*  Start RECO:  #*#*#*#*#*#*#*#*#*#*#*#*#*#*#*#*"
  python $TUTORIALROOT/share/tut_elec2rec.py  --profiling --profiling-details --no-db --evtmax $EVT_NUM > $curdir/output.rec
  echo "############## list after RECO  directory  ##################"
  ls -alhs
  # get the total rec event time
  rec_total1=`cat output.rec |grep "Sum of junotoptask"|awk '{print $5}'`
  # get the first rec event time 
  rec_event1=`cat junotoptas* |head -n 2|tail -n 1|awk '{print $9}'`
  # get the total rec event rec_total - first
  rec_totalms=`echo "$rec_total1-$rec_event1"|bc`
  rec_totals=$(echo "scale=3; $rec_totalms/1000"|bc)
  # output rec event time in second
  echo $rec_totals > $curdir/outputrec
  echo "RECO Time: $rec_totals"
  
  mv junotoptas* outputrecprofile
  rm junotoptask* -f
  status=$?

  # Set to 1 to print workload output to screen
  debug=0
  if [ $debug -eq 1 ]; then
    cd $curdir;
    echo "_________________________________________________OUTPUT START_________________________________________________";
    cat output;
    echo "_________________________________________________OUTPUT END_________________________________________________";
  fi 

  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Dummy version: accept user inputs as they are
  if [ "$USER_NCOPIES" != "" ]; then NCOPIES=$USER_NCOPIES; fi
  if [ "$USER_NTHREADS" != "" ]; then NTHREADS=$USER_NTHREADS; fi
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Return 0 if input arguments are valid, 1 otherwise
  return 0
}

# Optional function usage_detailed may be defined in each benchmark
# Input arguments: none
# Return value: none
function usage_detailed(){
  echo "NCOPIES*NTHREADS may be lower or greater than nproc=$(nproc)"
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=50

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
